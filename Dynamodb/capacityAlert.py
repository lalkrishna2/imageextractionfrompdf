import boto3
import json

def get_tables_on_alert(table_details):
    dynamodb = boto3.client('dynamodb')

    tables = dynamodb.list_tables()['TableNames']
    
    tables_on_alert = []

    for table in tables:
        response = dynamodb.describe_table(TableName = table)['Table']

        actual_read = response['ProvisionedThroughput']['ReadCapacityUnits']
        actual_write = response['ProvisionedThroughput']['WriteCapacityUnits']

        global_secondary_indexes = response['GlobalSecondaryIndexes']

        for GSI in global_secondary_indexes:
            actual_read += GSI['ProvisionedThroughput']['ReadCapacityUnits'] 
            actual_write += GSIresponse['ProvisionedThroughput']['WriteCapacityUnits']
        
        expected_read = tables_details[table]['ReadCapacity']
        expected_write = table_details[table]['WriteCapacity']


        if actual_read < expected_read or actual_write < expected_write:
            tables_on_alert += [table]
       
    return tables_on_alert















