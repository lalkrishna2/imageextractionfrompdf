import json
import cv2
import fitz
import django.conf import settings
import os
import boto3
import datetime 
base_dir = settings.BASE_DIR

def dhash(image_path, hashSize=8):
    image = cv2.imread(image_path)
    if image is None:
        return 0
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    resized = cv2.resize(image, (hashSize + 1, hashSize))
    diff = resized[:, 1:] > resized[:, :-1]
    return sum([2 ** i for (i, v) in enumerate(diff.flatten()) if v])

def get_images_from_pdf(path, password, base_dir = None):
    try:
        doc = fitz.open(path)
    except RuntimeError:
        print("Unable to extract images")
        return ({}, False)
    if doc.needsPass and doc.authenticate(password = password) == 0:
        print ("Authentication failed.")
        return ({}, True)
        
    images = []
    
    try:
        images = doc.getPageImageList(0)
    except RuntimeError:
        print("Unable to extract images")
        return ({}, False)
           
    # this will contains all hash values        
    hashes_list = []
    # this is a dictionary which contains whose key is png_name ans value is hash value of image
    x = {}
    
    var = datetime.datetime.now().strftime("%d-%m-%Y  %H-%M"))
    image_file = "images_from_pdf{}.zip".format(var)

    for img in images:
        
        xref = img[0]
        pix = fitz.Pixmap(doc, xref)
        png_name = "{}/{}-{}.png".format(base_dir, (path.split('/')[-1]).split('.')[0], xref)
        
        try:
            if pix.n < 5:
                pix.writePNG(png_name)
            else:
                pix1 = fitz.Pixmap(fitz.csRGB, pix)
                pix1.writePNG(png_name)
                
            hash_image = dhash(png_name)        
            
            if hash_image not in hashes_list:
                hashes_list += [hash_image]
                
                with open("/tmp/{}".format(image_file), "w") as zip:
                    zip.write(img)
                    
                x[png_name] = hash_image
                
                if os.path.exists(png_name):
                    os.remove(png_name)
                    
        except RuntimeError:
            # skip image for which couldn't write PNG file
            pass
        
    doc.close() # close the fitz document after work is done
    
    
   return (x, image_file, False)

def get_image_helper(file_path, password = ''):
    (image_hash_map, is_auth_failed) = get_images_from_pdf(path = file_path, password = password, base_dir = "{}/images".format(base_dir))
    
    if is_auth_failed:
        return False
    
    s3 = boto3.client('s3')
    
    s3.upload(("/tmp/{}").format(image_file),"images-from-pdf")
    
    url_for_images_from_pdf = s3.generate_presigned_url("get_object",Params = {"Bucket":bucket,"key":image_file}, ExpiredIn=500)
    
    var = datetime.datetime.now().strftime("%d-%m-%Y  %H-%M"))

    image_hash_file = "image_hash_map{}.zip".format(var)
    with open(("/tmp/{}").format(image_hash_file),  "w") as f:
        json.dumps(image_hash_map,f)
        
    s3.upload("/tmp/{}".format(image_hash_file),"images-from-pdf")        
    url_for_images_hash = s3.generate_presigned_url("get_object",Params = {"Bucket":bucket,"key":image_hash_file}, ExpiredIn=500)

    return True

def lambda_handler(event, context):
    
    is_auth_failed = get_image_helper(file_path,password)
    
    return {
        'statusCode': 200,
        if is_auth_failed:
            'body': json.dumps({"url_for_images_from_pdf":url_for_images_from_pdf,
                            "url_for_image_hash":url_for_image_hash}
                           )
        else:
            'body': json.dumps("Authentication falied")                   
    }
